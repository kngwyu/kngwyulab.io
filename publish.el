;;; publish.el --- Publish org-mode project on Gitlab Pages
;; Original Author: Rasmus
;; Author: kngwyu
;;; Commentary:
;;; Original file is in https://gitlab.com/pages/org-mode.
;;; I changed a bit to adjust to my page and use pygments.

(require 'org)
(require 'ox-publish)

;;; Code:

;;; Syntax highlighting using pygmentize
(define-advice org-html-src-block (:override (src-block _contents info) pygmentize)
  "Highlight src-block via Pygmentize."
  (let ((lang (org-element-property :language src-block))
        (code (car (org-export-unravel-code src-block))))
    (with-temp-buffer
      (call-process-region code nil "pygmentize" nil t nil "-l" lang "-f" "html")
      (buffer-string))))

(setq user-full-name "kngwyu")
(setq user-mail-address "yuji.kngw.80s.revive@gmail.com")

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-doctype "html5")
(defvar site-attachments (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                                       "ico" "cur" "css" "js" "woff" "html" "pdf")))
(setq org-publish-project-alist
      (list
       (list "contents"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "index"))
             :auto-sitemap t
             :sitemap-filename "index.org"
             :sitemap-title "Contents"
             :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../worg.css\"/>"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "sitemap"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README"))
             :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"./worg.css\"/>")
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("contents" "sitemap"))))
