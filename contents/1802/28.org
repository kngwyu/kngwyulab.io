#+TITLE: 適当にメモ
#+DATE: <2018-02-28 Wed>

* 合計を計算するマクロ×2
#+BEGIN_SRC rust
macro_rules! sum1 {
    ($x:expr) => ($x);
    ($x:expr, $($y:expr),*) => (sum1!($($y), *) + $x);
}
macro_rules! sum2 {
    ( $($x:expr)*) => ({
        let mut tmp = 0;
        $(tmp += $x;)*
        tmp
    })
}
fn main() {
    let a = sum1!(5, 6, 7, 8, 9);
    let b = sum2!(5 6 7 8 9);
    println!("{:?}", a);
    println!("{:?}", b);
}
#+END_SRC

再帰しない場合は,を区切り文字にできない……と思うけど間違ってるかも。
[[https://play.rust-lang.org/?gist=d9a2a0e1c1a37c8bc033664b5735eded&version=stable][playground]]

* Generators

https://doc.rust-lang.org/nightly/unstable-book/language-features/generators.html

これ後で調べとこうと思ったけど正直あんまこういうの好きじゃないんだよな。
どうしてもって時じゃないとあんま使いたくない。

* impl From for Option

#+BEGIN_SRC rust
#[stable(since = "1.12.0", feature = "option_from")]
impl<T> From<T> for Option<T> {
    fn from(val: T) -> Option<T> {
        Some(val)
    }
}
#+END_SRC

これ邪魔なんだけど...

と思ったけど自作エラー型とOptionの変換は

#+BEGIN_SRC rust
/// The error type that results from applying the try operator (`?`) to a `None` value. If you wish
/// to allow `x?` (where `x` is an `Option<T>`) to be converted into your error type, you can
/// implement `impl From<NoneError>` for `YourErrorType`. In that case, `x?` within a function that
/// returns `Result<_, YourErrorType>` will translate a `None` value into an `Err` result.
#[unstable(feature = "try_trait", issue = "42327")]
#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub struct NoneError;

#[unstable(feature = "try_trait", issue = "42327")]
impl<T> ops::Try for Option<T> {
    type Ok = T;
    type Error = NoneError;

    fn into_result(self) -> Result<T, NoneError> {
        self.ok_or(NoneError)
    }

    fn from_ok(v: T) -> Self {
        Some(v)
    }

    fn from_error(_: NoneError) -> Self {
        None
    }
}
#+END_SRC

を使えばいいのかな？

https://github.com/rust-lang/rust/issues/33417

* GAT

https://github.com/rust-lang/rust/issues/44265

全然進んでないなあ

